<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Accueil</title>
    <link rel="stylesheet" href="./styles/index.css?t=<? echo time(); ?>" media="all">
    <link rel="stylesheet" href="./structures/header.css">
    <link rel="icon" type="image/png" href="./photo/icone.png" />
    <link rel="stylesheet" href="./structures/footer.css">
    <link rel="stylesheet" href="./styles/mediaQueries.css">
</head>

<body>
    <?php
    include './structures/header.php';
    ?>
    <img src="./photo/patient.jpg" class="mediaQ patientQ" alt="photo d'une consultation ostéopathique">
    <div id="carrousel">
        <ul>
            <li><img class="pano" src="./photo/cabinet.jpg" /></li>
            <li><img class="pano" src="./photo/parking.jpg" /></li>
            <li><img class="pano" src="./photo/phototable.jpg" /></li>
        </ul>
    </div>
    <div>


        <!-- on inclut la bibliothèque depuis les serveurs de Google -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <script type="text/javascript" src="carousel.js"></script>

        <a href="./site/pourquoiConsulter.php">
            <div class="bouton">
                <p class="pk">Pourquoi consulter ?</p>
                <p class="rep">Au moins 10 raisons de venir … </p>
            </div>
        </a>
        <div class="contenaire">
            <div class="saviez-vous">
                <p class="titre">LE SAVIEZ-VOUS ?</p> <br>

                <p class="text"> <b>L’ostéopathie </b> est une technique thérapeutique, apparue au XIXe siècle aux
                    Etats- Unis. <br>

                    <p class="text">Fondée par le <b>Dr Andrew Taylor Still</b> sur des manipulations osseuses ou
                        musculaires. <br></p>

                    <p class="text"> La pensée première étant de considérer, que le bien-être du corps humain passait par le
                        bon fonctionnement de son appareil locomoteur. <br></p>

                    <p class="text">Envie d'en savoir plus ?<b><a href="https://www.doctolib.fr/osteopathe/balma/maxime-nicolaieff"> Prenez
                                rendez-vous !</a></b></p>
                </p>
            </div>
            <div class="img">
                <img src="./photo/rdv.png" alt="">
                <a href="https://www.doctolib.fr/osteopathe/balma/maxime-nicolaieff"><img src="./photo/doctolib.png" alt=""></a>
                <p class="image">
                    <img class="urgence2" src="./photo/urgence.png" alt="">
                    <span class="urgence">05.61.24.25.87</span>
                </p>
            </div>
        </div>
        <div class="connexion">
            <a href="./formulaire/connexion.php">
                Connexion &nbsp;
            </a>
            <a href="./formulaire/inscription.php">
                Inscription
            </a>

        </div>
        <br>
    </div>

    <?php
    include './structures/footer.php';
    ?>


</body>

</html>
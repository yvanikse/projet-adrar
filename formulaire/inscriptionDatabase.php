<?php
session_start();

if (isset($_POST['submit'])) {
    $Civilite = htmlspecialchars($_POST['Civilite']);
    $Prenom = htmlspecialchars($_POST['Prenom']);
    $Nom = htmlspecialchars($_POST['Nom']);
    $mdp = htmlspecialchars($_POST['mdp']);
    $Telephone = htmlspecialchars($_POST['Telephone']);
    $Mobile = htmlspecialchars($_POST['Mobile']);
    $Mail = htmlspecialchars($_POST['Mail']);
    $Naissance = htmlspecialchars($_POST['Naissance']);

    include 'data.php';
    //$db = new PDO('mysql:host=localhost;dbname=osteopathe', 'root', '');

    $stmt = $db->prepare("SELECT * FROM inscription WHERE mail=?");
    $stmt->execute([$Mail]);
    $user = $stmt->fetch();
    if ($user) {
        // email existe deja
        echo "E-mail deja utilisé";
    } else {
        // email n'existe pas 
        $mdp = sha1($mdp); //hachage a changer pour password_hash si temps dispo
        $sql = "INSERT INTO inscription (civilite, prenom, nom, mdp, telephone, mobile, mail, dateNaissance) VALUES ('$Civilite','$Prenom','$Nom','$mdp', '$Telephone',' $Mobile', '$Mail', '$Naissance')";
        $req = $db->prepare($sql);
        $req->execute();        # cette partie crée un utilisateur si il n'existe pas

        // header('Location: inscription.php'); // gere la redirection a la connexion

        echo "Enregistrement effectué";
    }
}

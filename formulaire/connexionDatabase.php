<?php
session_start();

include 'data.php';
//$db = new PDO('mysql:host=localhost;dbname=osteopathe', 'root', '');

if (isset($_POST['formconnexion'])) {

    $mailconnect = htmlspecialchars($_POST['mailconnect']);
    $mdpconnect = sha1($_POST['mdpconnect']); // fonction de hachage 
    if (!empty($mailconnect) and !empty($mdpconnect)) {

        $requser = $db->prepare("SELECT * FROM inscription WHERE mail = ? AND mdp = ?");
        $requser->execute(array($mailconnect, $mdpconnect));
        $userexist = $requser->rowCount();
        if ($userexist == 1) {

            $userinfo = $requser->fetch();
            $_SESSION['id'] = $userinfo['id'];
            $_SESSION['mail'] = $userinfo['mail'];
            include "connexion.php";
        } else {

            echo " Mauvais mail ou mot de passe";
        }
    } else {

        echo " tout les champs doivent etres complétés ! ";
    }
}

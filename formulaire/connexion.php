<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Connexion</title>
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="header.css">
    <link rel="stylesheet" href="connexon.css">
    <link rel="stylesheet" href="../structures/footer.css">

</head>

<body>
    <?php
    include "header.php";
    ?>

    <div class="formulaire">
        <form method="post" action="connexionDatabase.php">
            <p>
                <label>Mot de passe</label><input type="text" name="mdpconnect" required />
            </p>
            <p>
                <label>Mail</label><input type="email" name="mailconnect" required />
            </p>
            <div class="button">
                <button type="submit" name="formconnexion">Connexion</button>
            </div>
        </form>
    </div>

    <?php
    /* $db = new PDO('mysql:host=localhost;dbname=osteopathe', 'root', '');

    $reponse = $db->query("SELECT AVG(note) FROM commentaire");
    $donnes = $reponse->fetch();
    echo $donnes['AVG(note)'];*/

    if (isset($_SESSION['id'])) {
    ?>
        <div class="formulaire">
            <form method="post" action="commentaireDatabase.php">
                <p>
                    <label>Prénom</label><input type="text" name="prenom" required />
                </p>

                <p>
                    <label for="note">Note</label>
                    <select name="note">
                        <option name="note">1</option>
                        <option name="note">2</option>
                        <option name="note">3</option>
                        <option name="note">4</option>
                        <option name="note">5</option>
                    </select>
                </p>

                <label>Commentaire</label> <textarea name="commentaire" id="" cols="30" rows="10"></textarea>
                <div class="button">
                    <button type="submit" name="submit">Enregistrer</button>
                </div>
            </form>
        </div><?php
            }

                ?>

    <?php
    include "footer.php";
    ?>
</body>

</html>
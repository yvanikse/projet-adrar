<?php session_start(); ?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Inscription</title>
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="header.css?t=<? echo time(1); ?>" media="all">
    <link rel="stylesheet" href="inscriptio.css?t=<? echo time(1); ?>" media="all">
    <link rel="stylesheet" href="../structures/footer.css?=<? echo time(1); ?>" media="all">

</head>

<body>
    <?php
    include "header.php";
    include 'funMdp.php';
    ?>

    <div class="formulaire">
        <form method="post" action="inscriptionDatabase.php">

            <p>
                <label for="Civilite">Civilité</label>
                <select name="Civilite">
                    <option name="Civilite">Madame</option>
                    <option name="Civilite">Monsieur</option>

                </select>
            </p>
            <p>
                <label>Prénom</label><input type="text" name="Prenom" required />
            </p>
            <p>
                <label>Nom</label><input type="text" name="Nom" required />
            </p>
            <p>
                <label>Mot de passe</label><input type="text" name="mdp" placeholder="<?php echo $mon_mot_de_passe; ?>" required />
            </p>
            <p>
                <label>Téléphone</label><input type="text" name="Telephone" required />
            </p>
            <p>
                <label>Mobile</label><input type="text" name="Mobile" required />
            </p>
            <p>
                <label>Mail</label><input type="email" name="Mail" required />
            </p>
            <p>
                <label>Date de naissance</label><input type="date" name="Naissance" required />
            </p>
            <div class="button">
                <button type="submit" name="submit">Enregistrer</button>
            </div>
        </form>
    </div>
    <?php
    include "footer.php";
    ?>


</body>

</html>
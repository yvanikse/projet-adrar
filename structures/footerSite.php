<footer>
    <div class="tel"><img src="../photo/tel.png" alt="icone d'un telephone">
        <p>05.61.24.25.87</p>
    </div>
    <div class="map"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d16341.46692435198!2d1.4955433512480922!3d43.61090309811753!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe2d612fa9ffc03fb!2sMaxime%20Nicolaieff%20-%20ost%C3%A9opathe!5e0!3m2!1sfr!2sfr!4v1601209275096!5m2!1sfr!2sfr"></iframe>
    </div>
    <div class="position"><img src="../photo/map.png" alt="icone d'un selecteur de map">
        <p>8 route de Mons 31130 <b>Balma</b> </p>
    </div>
</footer>
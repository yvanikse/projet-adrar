<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Pourquoi consulter</title>
    <link rel="stylesheet" href="../styles/pourquoiConsulter.css">
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="../structures/header.css">
    <link rel="stylesheet" href="../structures/footer.css">
    <link rel="stylesheet" href="../styles/mediaQueriesConsulter.css">

</head>

<body>
    <?php
    include '../structures/headerSite.php';
    ?>
    <img class="squelette" src="../photo/corp.png" alt="photo d'un corps humain">
    <div class="squeletteQ">
        <img src="../photo/corpH.png" alt="photo d'un squelette">
        <img src="../photo/corpb.png" alt="photo d'un squelette">
        <img src="../photo/corpp.png" alt="photo d'un squelette">
    </div>

    <?php
    include '../structures/footerSite.php';
    ?>
</body>

</html>
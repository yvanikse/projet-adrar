<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Tarifs et Remboursements</title>
    <link rel="stylesheet" href="../styles/tarif.css">
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="../structures/header.css">
    <link rel="stylesheet" href="../structures/footer.css">
    <link rel="stylesheet" href="../styles/mediaQueriesTarifs.css">

</head>

<body>
    <?php
    include '../structures/headerSite.php';
    ?>
    <img class="tarif" src="../photo/tarif.png" alt="tarifs de votre ostéopathe">
    <a href="https://osteodiez.fr/rendez-vous-tarifs/liste-des-mutuelles-remboursant-les-consultations-dosteopathie/">
        <img class="assurance" src="../photo/mutuel1.png" alt=""></a>

    <?php
    include '../structures/footerSite.php';
    ?>
</body>

</html>
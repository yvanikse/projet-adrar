<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>F.A.Q</title>
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="../styles/FAQ.css">
    <link rel="stylesheet" href="../structures/header.css">
    <link rel="stylesheet" href="../structures/footer.css">
    <link rel="stylesheet" href="../styles/mediaQueriesFaq.css">

</head>

<body>
    <?php
    include '../structures/headerSite.php';
    ?><br>
    <div class="faq">
        <ul class="dropdownmenu">

            <li><a href="#" class="question">👉 Faut-il se déshabiller chez l'ostéopathe ?
                </a>
                <ul>
                    <div class="baba">
                        <li><a class="textfaq" href="#">Libre à vous de vous mettre en sous-vêtements pour l’examen ou
                                non.
                                Cependant il est
                                utile au praticien de pouvoir
                                examiner votre peau, votre posture, ainsi que la façon dont votre corps réagit au
                                traitement
                                ostéopathique. Certains
                                patients peuvent préférer rester habillé pour l’examen, mais le rendu risque d’être
                                moins
                                optimum.
                            </a></li>
                    </div>

                </ul>
            </li>
            <li><a href="#" class="question">👉 Faut-il amener les examens complémentaires ?
                </a>
                <ul>
                    <div class="baba">
                        <li><a class="textfaq" href="#">Oui, vous devez apporter vos derniers examens complémentaires.
                                En
                                effet vous pouvez
                                estimez qu’ils n’ont pas de
                                rapport direct avec votre motif de consultation, cependant ils sont une aide non
                                négligeable
                                à la définition de votre
                                traitement. Dans le doute munissez-vous du maximum d’informations possibles et laissez
                                le
                                praticien juger de l’utilité
                                des documents.

                            </a></li>
                    </div>
                </ul>
            </li>
            <li><a href="#" class="question">👉 Pouvez-vous intervenir également au sein de mon entreprise ?

                </a>
                <ul>
                    <div class="baba">
                        <li><a class="textfaq" href="#">Oui, je peux me déplacer en entreprise, le tarif « domicile »
                                est
                                alors pris en compte. Ce tarif peut être dégressif si plusieurs
                                salariés de la même entreprise sont intéressés par une consultation le même jour.
                            </a></li>
                    </div>
                </ul>
            </li>
            <li><a href="#" class="question">👉 Dois-je consulter un médecin avant de voir l'ostéopathe ?
                </a>
                <ul>
                    <div class="baba">
                        <li><a class="textfaq" href="#">Il n’y a pas forcement de nécessité à consulter un médecin
                                généraliste avant de venir au cabinet. Dans certains cas, je
                                pourrais demander un avis médical avant de vous prendre en charge (blessures récentes,
                                fracture non réparée…).

                            </a></li>
                    </div>
                </ul>
            </li>
            <li><a href="#" class="question">👉 L'ostéopathe va-t-il me faire craquer ?

                </a>
                <ul>
                    <div class="baba">
                        <li><a class="textfaq" href="#">Le craquement n’est pas un nécessité, d’autres méthodes sont
                                possibles et dites plus « douces ». Dans certain cas, les
                                techniques structurelles (avec craquement) peuvent être utilisées pour « libérer » une
                                articulation. Néanmoins cette
                                technique reste indolore.
                            </a></li>
                    </div>
                </ul>
            </li>
            <li><a href="#" class="question">👉 Ostéopathe ou kinésithérapeute ?

                </a>
                <ul>
                    <div class="baba baba2">
                        <li><a class="textfaq" href="#">Ces 2 professions sont souvent apparentées ou confondues, à
                                tort.
                                L’ostéopathie peut intervenir n’importe quand dans la vie d’un individu. Même quand il
                                n’y a
                                pas de douleur. L’ostéopathe
                                par ses techniques cherche, à réharmoniser le corps. Tous les troubles fonctionnels
                                mécaniquement réversibles entrent
                                dans le champ d’action de l’ostéopathie.
                                La kinésithérapie a une vocation première à la rééducation, même si son champ d’actions
                                ne
                                se limite pas à cela. Ces deux
                                professions peuvent être complémentaires.

                            </a></li>
                    </div>
                </ul>
            </li>
            <li><a href="#" class="question">👉 C'est pire après la consultation, que faire ?


                </a>
                <ul>
                    <div class="baba">
                        <li><a class="textfaq" href="#">A la fin de la séance il se peut que vous soyez un peu
                                courbaturés
                                pendant les 24 à 48h qui suivent le traitement.
                                Pas d’inquiétude c’est normal, votre corps se rééquilibre.
                                Hydrater vous bien. Un peu de repos est nécessaire, mais gardez quand même une activité.
                                Toute fois si la douleur persiste trop n’hésitez pas à reprendre contact avec votre
                                ostéopathe.
                            </a></li>
                    </div>
                </ul>
            </li>

            <li><a href="#" class="question">👉 Combien de séances ?</a>
                <ul>
                    <div class="baba">
                        <li><a class="textfaq" href="#">Généralement une séance peut suffire. Mais pour certains motifs
                                de consultation, une à deux séances supplémentaires peuvent être nécessaire. Cela se
                                fait au cas par cas.

                            </a></li>
                    </div>
                </ul>
            </li>
            <li><a href="#" class="question">👉 Les bons gestes à adopter après la séance ?

                </a>
                <ul>
                    <div class="baba ">
                        <li><a class="textfaq" href="#">Hydratez-vous bien.
                                Reposez-vous.
                                Vous pouvez pratiquer une activité physique, en respectant un délai d’environ 24h et
                                surtout
                                votre corps.
                                Etirements si, conseillés par votre ostéopathe.
                                Améliorer votre posture au travail, avec les indications données par votre ostéopathe.
                                D’autres conseils peuvent vous être donnés, suivant votre cas.

                            </a></li>
                    </div>
                </ul>
            </li>
        </ul>
    </div>
    <!--<div class="avis"><a href="https://www.google.com/maps/place/Maxime+Nicolaieff+-+ost%C3%A9opathe,+8+Route+de+Mons,+31130+Balma/@43.6097138,1.5000201,15z/data=!4m2!3m1!1s0x12aebd93593ab34b:0xe2d612fa9ffc03fb">
            <img src="../photo/etoile.png" alt="photo d'une etoile">
        </a></div>-->
    <div class="avis">
        <?php
        include '../formulaire/data.php';

        $response = $db->query("SELECT AVG(note) AS note_moyenne FROM commentaire;");
        $donnees = $response->fetch();
        //echo $donnees['note_moyenne'];

        if ($donnees['note_moyenne'] < 0.9) {
        ?> <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
        <?php
        } elseif ($donnees['note_moyenne'] > 0.91 and $donnees['note_moyenne'] < 1.90) {
        ?> <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
        <?php
        } elseif ($donnees['note_moyenne'] > 1.91 and $donnees['note_moyenne'] < 2.90) {
        ?> <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
        <?php
        } elseif ($donnees['note_moyenne'] > 2.91 and $donnees['note_moyenne'] < 3.90) {
        ?> <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileVide.png" alt="etoile noir">
            <img src="../photo/etoileVide.png" alt="etoile noir">
        <?php
        } elseif ($donnees['note_moyenne'] > 3.91 and $donnees['note_moyenne'] < 4.90) {
        ?> <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileVide.png" alt="etoile noir">
        <?php
        } else {
        ?><img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
            <img src="../photo/etoileRempli.png" alt="etoile jaune">
        <?php
        }
        //on recuperer le contenue de la table commentaire
        $reponse = $db->query('SELECT * FROM commentaire');

        //on affiche chaque entrée une a une
        while ($donnes = $reponse->fetch()) {
        ?>
            <p>
                <strong>Pseudo :</strong><?php echo $donnes['prenom']; ?> <br />
                <strong>Commentaire :</strong><?php echo $donnes['commentaire']; ?> <br />
                <strong>Commentaire posté le :</strong><?php echo $donnes['date_com'] ?>
            </p>
        <?php
        } ?></div>
    <?php
    $reponse->closeCursor(); // Termine le traitement de la requete


    include '../structures/footerSite.php';
    ?>



</body>

</html>
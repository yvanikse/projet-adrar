<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Devenir partenaire</title>
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="../styles/Partenaire.css">
    <link rel="stylesheet" href="../structures/header.css">
    <link rel="stylesheet" href="../structures/footer.css">
    <link rel="stylesheet" href="../styles/mediaQueriesPartenaire.css">
</head>

<body>
    <?php
    include '../structures/headerSite.php';
    ?>

    <img class="partenaire" src="../photo/partenaire.png" alt="partenaria ostéopathie">

    <?php
    include '../structures/footerSite.php';
    ?>
</body>

</html>
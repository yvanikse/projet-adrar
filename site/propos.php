<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>A propos</title>
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="../styles/propos.css">
    <link rel="stylesheet" href="../structures/header.css">
    <link rel="stylesheet" href="../structures/footer.css">
    <link rel="stylesheet" href="../styles/mediaQueriesPropos.css">

</head>

<body>
    <?php
    include '../structures/headerSite.php';
    ?>
    <img class="propos" src="../photo/propos.png" alt="graphique d'un ostéopathe">


    <?php
    include '../structures/footerSite.php';
    ?>
</body>

</html>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Votre ostéo</title>
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="../styles/votreOstéo.css">
    <link rel="stylesheet" href="../structures/header.css">
    <link rel="stylesheet" href="../structures/footer.css">
    <link rel="stylesheet" href="../styles/mediaQueriesOstéo.css">

</head>

<body>
    <?php
    include '../structures/headerSite.php';
    ?>
    <img class="votreOstéo" src="../photo/votreostéo.png" alt="photo de presentation de votre ostéopathe">


    <?php
    include '../structures/footerSite.php';
    ?>
</body>

</html>
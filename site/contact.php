<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Contact</title>
    <link rel="icon" type="image/png" href="../photo/icone.png" />
    <link rel="stylesheet" href="../styles/contact.css">
    <link rel="stylesheet" href="../structures/header.css">
    <link rel="stylesheet" href="../structures/footer.css">
    <link rel="stylesheet" href="../styles/mediaQueriesContact.css">
</head>

<body>
    <?php
    include '../structures/headerSite.php';
    ?>
    <img class="contact" src="../photo/contact.png" alt="contact de votre ostéopathe">
    <a class="linki" href="https://www.linkedin.com/in/maxime-nicolaïeff-baa18b1b7/"><img src="../photo/linki.png" alt="photo du logo de linkedin"></a>

    <?php
    include '../structures/footerSite.php';
    ?>
</body>

</html>